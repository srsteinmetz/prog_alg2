from Aluno.Aluno import Aluno

class AlunoEnsinoMedio(Aluno):

    def __init__(self, ano):
        self.ano = ano

    def imprimir(self):
        print("Ano: " + self.ano)
        