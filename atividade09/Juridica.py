from Pessoa import Pessoa

class Juridica:

    def __init__(self, CNPJ, inscricaoEstadual, quantidadeFuncionarios):
        self.CNPJ = CNPJ
        self.inscricaoEstadual = inscricaoEstadual
        self.quantidadeFuncionarios = quantidadeFuncionarios

    def setCNPJ(self, CNPJ):
        self.CNPJ = CNPJ

    def setInscricaoEstadual(self, inscricaoEstadual):
        self.inscricaoEstadual = inscricaoEstadual

    def setQuantidadeFuncionarios(self, quantidadeFuncionarios):
        self.quantidadeFuncionarios = quantidadeFuncionarios

    def getInscricaoEstadual(self):
        return self.inscricaoEstadual

    def getCNPJ(self):
        return self.CNPJ

    def getQuantidadeFuncionarios(self):
        return self.quantidadeFuncionarios


