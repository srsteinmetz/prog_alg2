from Pessoa import Pessoa


class Fisica:

    def __init__(self, cpf, idade, peso, altura):
        self.cpf = cpf
        self.idade = idade
        self.peso = peso
        self.altura = altura

    def setCpf(self, cpf):
        self.cpf = cpf

    def setIdade(self, idade):
        self.idade = idade

    def setPeso(self, peso):
        self.peso = peso

    def setAltura(self, altura):
        self.altura = altura

    def getCpf(self):
        return self.cpf

    def getIdade(self):
        return self.idade

    def getPeso(self, peso):
        return self.peso

    def getAltura(self):
        return self.altura