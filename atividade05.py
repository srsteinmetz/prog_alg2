class Pilha:

    def __init__(self):
        self.__pilha = []

        def __len__(self):
            return len(self.__pilha)

        def is_empty(self):
            return len(self.__pilha) == 0

        def push(self, element):
            self.__pilha.append(element)

        def pop(self):
            if (self.is_empty()):
                print('Pilha está vazia')
            else:
                return self.__pilha.pop()

        def top(self):
            if (self.is_empty()):
                print('Pilha está vazia')
            else:
                return self.__pilha[-1]

    pilha = Pilha()

    if pilha.is_empty():
        print("pilha vazia")

    pilha.push(5)
    pilha.push(6)
    pilha.push(7)

    print(pilha.pop())

    print("primeiro elemento")
    print(pilha.top())

    print("tamanho da pilha")
    print(len(pilha))