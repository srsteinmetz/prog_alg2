package backend;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package inspiredomus.Backend;

import java.awt.Dimension;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class GoogleHomePage {
   public static void main(String[] args) {
      createWindow();
      //createUI();
   }

   private static void createWindow() {    
      JEditorPane jep = new JEditorPane();
        jep.setEditable(false);   

        try {
          jep.setPage("http://www.google.com.br");
        }catch (IOException e) {
          jep.setContentType("text/html");
          jep.setText("<html>Could not load</html>");
        } 

        JScrollPane scrollPane = new JScrollPane(jep);     
        JFrame f = new JFrame("Test HTML");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(scrollPane);
        f.setPreferredSize(new Dimension(800,600));
        f.setVisible(true);
   }

} 