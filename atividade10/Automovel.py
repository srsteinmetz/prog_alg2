from Veiculo import Veiculo

class Automovel(Veiculo):

    def __init__(self, potenciaDoMotor):
        self.potenciaDoMotor = potenciaDoMotor

    def imprimirInformacoes(self):
        print("Informações: " + self.potenciaDoMotor)