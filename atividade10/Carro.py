from Veiculo import Veiculo
from Automovel import Automovel

class Carro(Automovel):

    def __init__(self, qtdPortas):
        self.qtdPortas = qtdPortas

    def imprimirInformacoes(self):
        print("Informações: " + self.qtdPortas)
