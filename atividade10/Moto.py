from Automovel import Automovel
from Veiculo import Veiculo

class Moto(Automovel):

    def __init__(self, partidaEletrica):
        self.partidaEletrica = partidaEletrica

    def imprimirInformacoes(self):
        print("Informações: " + self.partidaEletrica)